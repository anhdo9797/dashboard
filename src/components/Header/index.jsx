import React, { Component } from 'react';
import style from './style.scss';

export default class Header extends Component {
    render() {
        return (
            <div className="container">
                <header>
                    <h1>DASHBOARD</h1>
                    <ion-icon name="add-circle-outline"></ion-icon>
                </header>
            </div>
        );
    }
}
